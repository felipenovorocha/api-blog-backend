#  Portfolio Felipe Rocha - Backend


## Tabela de Conteúdos
<hr>

- [Tecnologias](#tecnologias)
- [Funcionalidades](#funcionalidades)
- [Screenshots](#screenshots)
- [Técnicas](#tecnicas)

<br>

## SOBRE
<hr>


### Web Service para backend focado em funcionalidades para criação, categorização e busca de artigos (blog)
 ### Visite o site.


<br>

<a name="tecnologias"></a>

## Tecnologias Utilizadas

<hr>
<br>


<div class ="icons">

<pre>
          <img width="60px" heigth="60px" class = "icon" src ="icons/database-solid.svg">          <img width="60px" heigth="60px" class = "icon" src ="icons/java-brands.svg">          <img width="80px" heigth="80px" class = "icon" src ="icons/spring-boot.svg">
</pre>




</div>



<br>

<a name="tecnicas"></a>

## Técnicas Utilizadas

<hr>

> - Modelo de Entidades Relacional
> - Mapeamento de entidades  
> - Mapeamento de Chaves Estrangeiras OneToMany
> - Mapeamento de Chaves Estrangeiras ManyToMany
> - Mapeamento de Chaves Compostas
> - Definição de regras de Negócio na camada de Serviços 
> - Definição de roteamentos na camada de Controllers
> - Tratamento de Erros




<br>

<a name="funcionalidades"></a>

## Funcionalidades

<hr>

### Blog :computer: <br> 

> - [x] Listagem de Posts 
> - [x] Listagem de Posts por Id 
> - [x] Cadastro de Postagens
> - [x] Exclusão de de Postagens
> - [x] Atualização de de Postagens
### Usuários :smiley: <br> 
> - [x] Cadastro 
> - [x] Cadastro atrelados à endereço 
> - [x] Atualização de dados 
> - [x] Exclusão de Usuário 


### Segurança :shield: <br>
> - [ ] User Roles
> - [ ] Sessions



<hr>


<br>
<br>

## **SCREENSHOTS**
<br>

<a name = "screenshots"> </a>
<div class="screenshots">


<div class="screenshot">

### **MAPEAMENTO DE CHAVE COMPOSTA**
<hr>
<img src ="screenshots/Chave Composta.JPG" >

</div>

<br>

<div class="screenshot">

### **MAPEAMENTO DE ENTIDADE**
<hr>


<img src ="screenshots/Entidade.JPG" >

</div>

<div class="screenshot">

### **SERVICE**
<hr>

<img src ="screenshots/Service.jpg" >

</div>

<div class="screenshot">

### **CONTROLLER**
<hr>

<img src ="screenshots/Controller1.jpg" >
<img src ="screenshots/Controller2.jpg" >

</div>
<div class="screenshot">

### **TRATAMENTO DE EXCEÇÕES**
<hr>

<img src ="screenshots/Tratamento de Exceções.jpg" >

</div>

</div>

[GITHUB]: http://github.com/felipenovorocha
[LINKEDIN]: https://www.linkedin.com/in/felipenovorocha/








