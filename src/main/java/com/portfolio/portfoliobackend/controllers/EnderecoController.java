package com.portfolio.portfoliobackend.controllers;



import com.portfolio.portfoliobackend.entities.Endereco;
import com.portfolio.portfoliobackend.services.EnderecoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class EnderecoController {

    @Autowired
    public EnderecoService service;


    @CrossOrigin
    @RequestMapping(value = "/endereco")
    @ResponseBody
    public Iterable<Endereco> listar(){

        return service.findAll();        

    }


    @CrossOrigin
    @PostMapping(value = "/endereco")
    @ResponseBody
    public void Inserir( @RequestBody Endereco endereco){
        service.Inserir(endereco);
    }

    
}
