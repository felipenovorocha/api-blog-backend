package com.portfolio.portfoliobackend.controllers;

import java.util.Optional;

import com.portfolio.portfoliobackend.entities.Post;
import com.portfolio.portfoliobackend.services.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class PostController {

    @Autowired
    public PostService service;

    @CrossOrigin
    @RequestMapping(value = "/post")
    @ResponseBody
    public Iterable<Post> listar() {
        try {
            return service.listar();
        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }

    @CrossOrigin
    @RequestMapping(value = "/post/{id}")
    @ResponseBody
    public Optional<Post> listarPorId(@PathVariable Integer id) {
        return service.listarPorId(id);

    }

    @CrossOrigin
    @PostMapping(value = "/post")
    @ResponseBody
    public void inserirPost(@RequestBody Post post){
        service.inserirPost(post);
    }
    

    @CrossOrigin
    @PutMapping(value = "/post/{id}")
    @ResponseBody
    public void editarPost(@PathVariable Integer id, @RequestBody Post post) throws Exception{
        // try {
            service.editarPost(post, id);
            // } catch (Exception e) {
                // e.printStackTrace();
                // }
                
            }
    
    @CrossOrigin
    @DeleteMapping(value = "/post/{id}")
    @ResponseBody
    public void deletarPost(@PathVariable Integer id){
        service.deletarPost(id);
    }






}
