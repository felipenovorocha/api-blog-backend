package com.portfolio.portfoliobackend.controllers;

import java.util.Optional;

import com.portfolio.portfoliobackend.entities.Autor;
import com.portfolio.portfoliobackend.services.AutorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class AutorController {
    @Autowired
    public AutorService service;
    
    @CrossOrigin
    @RequestMapping(value = "/autor")
    @ResponseBody
    public Iterable <Autor> listar(){
        try {
            
            return service.listar();
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            
        }
    }

    @CrossOrigin
    @PostMapping(value = "/autor")
    @ResponseBody
    public void inserir(@RequestBody  Autor autor){
        service.inserir(autor);
    }
    
    
    @CrossOrigin
    @RequestMapping(value = "/autor/{id}")
    @ResponseBody
    public Optional<Autor> listarPorId(@PathVariable Integer id){

        return service.listarPorId(id);
    }


    @CrossOrigin
    @PutMapping(value = "/autor/{id}")
    @ResponseBody
    public void editar(@RequestBody Autor autor, @PathVariable Integer id){
        service.editar(id, autor);
    }

    @CrossOrigin
    @DeleteMapping(value = "/autor/{id}")
    @ResponseBody
    public void deletar(@PathVariable Integer id ){
        service.deletar(id);
    }

}
