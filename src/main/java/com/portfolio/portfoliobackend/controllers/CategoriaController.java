package com.portfolio.portfoliobackend.controllers;

import com.portfolio.portfoliobackend.entities.Categoria;
import com.portfolio.portfoliobackend.services.CategoriaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api")
public class CategoriaController {
    @Autowired
    public CategoriaService service;


    @CrossOrigin
    @RequestMapping(value = "/categoria")
    @ResponseBody
    public Iterable<Categoria> listar(){
        try {
            return service.listar();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
}


}
