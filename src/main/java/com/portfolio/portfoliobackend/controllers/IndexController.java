package com.portfolio.portfoliobackend.controllers;

import com.portfolio.portfoliobackend.entities.Links;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexController {

    @GetMapping
    @RequestMapping("/")
    public Links Hello() {

        Links links = new Links();

        links.setAutores("/api/autor");
        links.setPosts("/api/post");
        links.setCategorias("/api/categorias");

        return links;
    }

    // @GetMapping(value = "/")

    // public Iterable<Post> findAllPosts() {
    // return postRepository.findAll();
    // }

}
