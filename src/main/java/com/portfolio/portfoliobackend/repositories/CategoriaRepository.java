package com.portfolio.portfoliobackend.repositories;

import com.portfolio.portfoliobackend.entities.Categoria;

import org.springframework.data.repository.CrudRepository;

public interface CategoriaRepository extends CrudRepository<Categoria, Integer> {
    
}
