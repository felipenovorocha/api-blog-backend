package com.portfolio.portfoliobackend.repositories;

import com.portfolio.portfoliobackend.entities.Endereco;
import com.portfolio.portfoliobackend.entities.EnderecoID;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EnderecoRepository  extends JpaRepository <Endereco, EnderecoID>{
    
}
