package com.portfolio.portfoliobackend.repositories;

import com.portfolio.portfoliobackend.entities.Post;

import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends JpaRepository<Post, Integer> {
  
}
