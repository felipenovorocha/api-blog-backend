package com.portfolio.portfoliobackend.repositories;

import com.portfolio.portfoliobackend.entities.Autor;

import org.springframework.data.repository.CrudRepository;

public interface AutorRepository extends CrudRepository <Autor, Integer>{
    
}
