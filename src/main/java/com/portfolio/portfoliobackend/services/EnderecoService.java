package com.portfolio.portfoliobackend.services;

import java.util.Optional;

import com.portfolio.portfoliobackend.entities.Endereco;
import com.portfolio.portfoliobackend.entities.EnderecoID;
import com.portfolio.portfoliobackend.repositories.EnderecoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService {

    @Autowired
    public EnderecoRepository repository;

    public Iterable<Endereco> findAll(){
        return repository.findAll();
    }

    public Optional<Endereco> findById(EnderecoID id){

        return repository.findById(id);
    }

    public void Inserir(Endereco endereco){

        repository.save(endereco);

    }

    public void Deletar(EnderecoID id){
        repository.deleteById(id);
    }

    
}
