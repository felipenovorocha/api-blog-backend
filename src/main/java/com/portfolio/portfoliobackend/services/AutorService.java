package com.portfolio.portfoliobackend.services;

import java.util.Optional;

import com.portfolio.portfoliobackend.entities.Autor;
import com.portfolio.portfoliobackend.repositories.AutorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AutorService {

    @Autowired
    public AutorRepository autorRepository;

    public Iterable<Autor> listar() {

        return autorRepository.findAll();
    }

    public Optional<Autor> listarPorId(Integer id) {
        return autorRepository.findById(id);
    }

    //post

    public void inserir(Autor autor){
        autorRepository.save(autor);
    }
    
    
    //put

    public void editar(Integer id, Autor autor){

        //consultar na base de dados
        Optional<Autor> autorDb = autorRepository.findById(id);

        //conferir se a consulta possui retorno
        if(autorDb.isPresent()){
            // se possuir retorno, então atribuir resultado a uma variável
            Autor novoAutor = autorDb.get();
            // atualizar atributos
            novoAutor.setNome(autor.getNome());
            novoAutor.setEmail(autor.getEmail());
            novoAutor.setCpf(autor.getCpf());
            novoAutor.setRg(autor.getRg());
            novoAutor.setDataNasc(autor.getDataNasc());
            novoAutor.setImg(autor.getImg());
            autorRepository.save(novoAutor);
        }
        else{
            System.out.println("Não foi possível atualizar");
        }
    
    }

    public void deletar(Integer id){
        autorRepository.deleteById(id);
    }
    
    
    
    //delete

}
