package com.portfolio.portfoliobackend.services;

import java.util.Optional;

import com.portfolio.portfoliobackend.entities.Categoria;
import com.portfolio.portfoliobackend.repositories.CategoriaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoriaService {
    
    @Autowired
    public CategoriaRepository repository;

    public Iterable<Categoria> listar(){

        return repository.findAll();

    }

    public Optional<Categoria> listarPorID(Integer id){
        return repository.findById(id);
    }

}
