package com.portfolio.portfoliobackend.services;

import java.util.Optional;

import com.portfolio.portfoliobackend.entities.Post;
import com.portfolio.portfoliobackend.repositories.PostRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostService {

    @Autowired
    public PostRepository repository;

    public Iterable<Post> listar() {

        return repository.findAll();
    }

    public Optional<Post> listarPorId(Integer id) {
        return repository.findById(id);
    }

    public void inserirPost(Post post){
         
         repository.save(post);
     
    }

    public void editarPost (Post post, int id) throws Exception{
        //contruir um objeto utilizando o find by id
        Optional<Post> postDb = repository.findById(id);

        //testar se a busca retornou algum resultado
        if (postDb.isPresent()){
            //se retornou algum restuldado, sobrescrever objeto com dados atualizados
            Post novoPost = postDb.get();
            novoPost.setTitulo(post.getTitulo());
            novoPost.setAutor(post.getAutor());
            novoPost.setConteudo(post.getConteudo());
            novoPost.setImg(post.getImg());

            repository.save(post);
        }

        else{
            System.out.println("Não foi possível atualizar");
        }
        //salvar novo objeto
    }

    
    public void deletarPost (int id){

        repository.deleteById(id);
    }
    

}
