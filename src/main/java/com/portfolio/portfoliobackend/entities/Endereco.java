package com.portfolio.portfoliobackend.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "endereco")

public class Endereco {

    @EmbeddedId
    public EnderecoID id;

    @Column(name="bairro")
    public String bairro;
    @Column(name="estado")
    public String uf;
    
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(referencedColumnName = "id_autor", insertable = false, updatable = false)
    public Autor autor;

    @Column(name="autor_id_autor")
    public Integer idAutor;
    
}
