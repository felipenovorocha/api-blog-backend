package com.portfolio.portfoliobackend.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "autor")
public class Autor {


@Id
@Column(name = "id_autor")
@GeneratedValue(strategy = GenerationType.IDENTITY)
public Integer idautor;

@Column(name = "nome")
public String nome;
@Column(name = "email")
public String email;
@Column(name = "data_nasc")
public Date dataNasc;
@Column(name = "cpf")
public String cpf;
@Column(name = "rg")
public String rg;
@Column (name = "img")
public String img;




}
