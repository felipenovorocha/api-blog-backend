package com.portfolio.portfoliobackend.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "categoria_has_post")
public class CategoriaHasPost {
    
    @Id
    @Column(name="categoria_id_categ")
    public int categoriaID;
    @Column(name="post_idpost")
    public int postID;
}
