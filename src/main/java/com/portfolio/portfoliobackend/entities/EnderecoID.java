package com.portfolio.portfoliobackend.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Embeddable
public class EnderecoID implements Serializable{

    public String logradouro;
    public String localidade;
    public Integer numero;
    
}
