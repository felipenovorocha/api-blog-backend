package com.portfolio.portfoliobackend.entities;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


import lombok.Data;

@Data
@Entity
@Table(name = "post")
public class Post implements Serializable  {

   
    @Id
    @Column(name = "idpost")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "titulo")
    public String titulo;
    
    @Column(name = "data")    
    public Date dataDeCriacao;
    
    
    @Column(name = "conteudo")
    public String conteudo;
    
    @Column(name = "img")
    public String img;

    @Column(name = "autor_id_autor")
    public Integer idAutor;


    @ManyToOne (cascade = CascadeType.DETACH )
    @JoinColumn(referencedColumnName = "id_autor", insertable = false, updatable = false)
    public Autor autor;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "categoria_has_post", joinColumns = @JoinColumn(name="idpost"),
         inverseJoinColumns = @JoinColumn(name="id_categ"))
    @JsonIgnore
    public List<Categoria> categorias;


}
