package com.portfolio.portfoliobackend.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table (name = "categoria")
public class Categoria {

    @Id
    @Column (name = "id_categ")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id_categ;

    @Column(name = "nome_categ")
    public String nome_categ;

    // @ManyToMany(targetEntity = Post.class, mappedBy = "categorias")
    // @JsonIgnore
    // public List<Post> posts;
    

    
}
